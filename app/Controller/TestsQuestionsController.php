<?php
App::uses('AppController', 'Controller');
App::uses('UsersAnswersController','Controller');
App::uses('AnswersController','Controller');
/**
 * TestsQuestions Controller
 *
 * @property TestsQuestion $TestsQuestion
 * @property PaginatorComponent $Paginator
 */
class TestsQuestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TestsQuestion->recursive = 0;
		$this->set('testsQuestions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TestsQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid tests question'));
		}
		$options = array('conditions' => array('TestsQuestion.' . $this->TestsQuestion->primaryKey => $id));
		$this->set('testsQuestion', $this->TestsQuestion->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TestsQuestion->create();
			if ($this->TestsQuestion->save($this->request->data)) {
				$this->Flash->success(__('The tests question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tests question could not be saved. Please, try again.'));
			}
		}
		$tests = $this->TestsQuestion->Test->find('list');
		$questions = $this->TestsQuestion->Question->find('list');
		$this->set(compact('tests', 'questions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TestsQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid tests question'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TestsQuestion->save($this->request->data)) {
				$this->Flash->success(__('The tests question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tests question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TestsQuestion.' . $this->TestsQuestion->primaryKey => $id));
			$this->request->data = $this->TestsQuestion->find('first', $options);
		}
		$tests = $this->TestsQuestion->Test->find('list');
		$questions = $this->TestsQuestion->Question->find('list');
		$this->set(compact('tests', 'questions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TestsQuestion->id = $id;
		if (!$this->TestsQuestion->exists()) {
			throw new NotFoundException(__('Invalid tests question'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TestsQuestion->delete()) {
			$this->Flash->success(__('The tests question has been deleted.'));
		} else {
			$this->Flash->error(__('The tests question could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TestsQuestion->recursive = 0;
		$this->set('testsQuestions', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TestsQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid tests question'));
		}
		$options = array('conditions' => array('TestsQuestion.' . $this->TestsQuestion->primaryKey => $id));
		$this->set('testsQuestion', $this->TestsQuestion->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TestsQuestion->create();
			if ($this->TestsQuestion->save($this->request->data)) {
				$this->Flash->success(__('The tests question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tests question could not be saved. Please, try again.'));
			}
		}
		$tests = $this->TestsQuestion->Test->find('list');
		$questions = $this->TestsQuestion->Question->find('list');
		$this->set(compact('tests', 'questions'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TestsQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid tests question'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TestsQuestion->save($this->request->data)) {
				$this->Flash->success(__('The tests question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tests question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TestsQuestion.' . $this->TestsQuestion->primaryKey => $id));
			$this->request->data = $this->TestsQuestion->find('first', $options);
		}
		$tests = $this->TestsQuestion->Test->find('list');
		$questions = $this->TestsQuestion->Question->find('list');
		$this->set(compact('tests', 'questions'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TestsQuestion->id = $id;
		if (!$this->TestsQuestion->exists()) {
			throw new NotFoundException(__('Invalid tests question'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TestsQuestion->delete()) {
			$this->Flash->success(__('The tests question has been deleted.'));
		} else {
			$this->Flash->error(__('The tests question could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
    /**
     * @param null $test_id
     * @param int $limit
     * @param int $offset
     */
    public function held_exam($test_id = null, $limit = 1, $offset = 0){
        $questions = $this->_getExamData($test_id, $limit,$offset, $this->get_user_id());
        $subject_id =  @$questions['Question']['subject_id'];
        $question_id =  @$questions['Question']['id'];
        $this->_common_held_exam($test_id, $limit, $offset);
    }
    /**
     * @param null $test_id
     * @param int $limit
     * @param int $offset
     */
    private  function _common_held_exam($test_id = null, $limit = 1, $offset = 0, $admin = false){
        #$this->TestsQuestion->Behaviors->load('Containable');
        if($admin) {
            $user_id = AuthComponent::user('id');
        } else {
            $user_id = $this->get_user_id();
        }
        $find = 'first';

        $questions = $this->_getExamData($test_id, $limit,$offset, $user_id, $find);
        $test_name = $questions['Test']['title'];
        $question_id = $questions['Question']['id'];
        $total_question = $this->_count_question_test_wise($test_id);
        $is_exist = $this->_is_flag($test_id, $question_id, $user_id, 'mock_test');
        if(!empty($is_exist['UsersAnswer']['is_flag'])){
            $red_flag = 'red-flag';
        } else{
            $red_flag = '';
        }
        $answer_id = null;
        if(!empty($is_exist['UsersAnswer']['answer_id'])){
            $answer_id = $is_exist['UsersAnswer']['answer_id'];
        }
        #App::uses('UsersAnswersController','Controller');
        /*Get Remaining Test Time*/
       /* App::uses('SaveandexitsController','Controller');
        $exit_obj = new SaveandexitsController();
        $remaining_test_time = $exit_obj -> getRemainingTime($user_id, $test_id);*/
        /*End Remaining time*/
        $obj = new UsersAnswersController();
        $all_answered = false;
        $my_answered_question = $obj -> no_of_answered_question($test_id, $user_id,'mock_test');
        $my_answered_question_count = count($my_answered_question);
        if($my_answered_question_count >= $total_question) {
            $all_answered = true;
            $all_questions = $obj-> get_all_questions_after_exam($test_id, $user_id, 'mock_test');
            $result = $this->_calculateResult($all_questions, $total_question);
            #AuthComponent::_setTrace($my_answered_question);
            $this->set(compact('all_questions','result'));
        } else{
            $submit_btn = count($my_answered_question)==$total_question-1 ? true : false;

            $all_flagged_questions =  Hash::extract($this-> get_all_flagged_questions($test_id,'mock_test'), '{n}.UsersAnswer.question_id');
            #$questions_testwise = Hash::extract($this-> _getQuestionByTest($test_id), '{n}.TestsQuestion.question_id');
            $questions_data_testwise = Hash::extract($this-> _getQuestionByTest($test_id), '{n}.TestsQuestion.question_id');
            $questions_testwise = $this->_create_jump_question_list($questions_data_testwise);
            //AuthComponent::_setTrace($questions_testwise);
            #AuthComponent::_setTrace($questions_testwise);
            /*AuthComponent::_setTrace($all_flagged_questions, false);*/
            #AuthComponent::_setTrace($my_answered_question);
            $this->set(compact('questions','offset','test_name','red_flag','all_flagged_questions','questions_testwise', 'submit_btn'));
        }
        $this->set(compact('all_answered','my_answered_question','test_id','answer_id','total_question','my_answered_question_count','remaining_test_time','find'));
    }
    private  function _getExamData($test_id , $limit, $offset, $user_id = null, $find = 'first'){
        $this->TestsQuestion->Behaviors->load('Containable');
        $query = [
            'conditions' => ['TestsQuestion.test_id' => $test_id,'Test.status' => 1],
            'contain' => [
                'Test'=>[
                    'fields' => [
                        'Test.id','Test.title','Test.test_time'
                    ]
                ],
                'Question'=>[
                    'fields' => [
                        'Question.id','Question.question','Question.answer_description','Question.solution','Question.subject_id',
                    ],
                    'Answer',
                ]
            ],
            'limit' => $limit,
            'offset' => $offset,
        ];
        return $this->TestsQuestion->find($find, $query);
    }
    private function _count_question_test_wise($test_id = null){
        return $this->TestsQuestion->find('count', array('recursive' => -1,'conditions' => array('TestsQuestion.test_id' => $test_id)));
    }
    /**
     * @param null $test_id
     * @param null $question_id
     * @param null $user_id
     * @param string $test_type
     * @return array|null
     */
    private function _is_flag($test_id = null, $question_id = null, $user_id = null, $test_type = ''){
        $this->loadModel('UsersAnswer');
        $query = [
            'recursive' => -1,
            'field' => ['UsersAnswer.is_flag','UsersAnswer.answer_id'],
            'conditions' => [
                'UsersAnswer.user_id' => $user_id,
                'UsersAnswer.test_id' => $test_id,
                'UsersAnswer.question_id' => $question_id,
                'UsersAnswer.test_type' => $test_type,
            ],
        ];
        return $this->UsersAnswer->find('first',$query);
    }

    /**
     * @param array $all_questions
     * @param $total_question
     * @return float|int
     */
    private function _calculateResult($all_questions = [], $total_question)
    {
        $correct = 0;
        $result = 0;
        foreach ($all_questions as $key => $question) {
            $sl_no = $key + 1;
            if(!empty($question['Question']['Answer'])) {
                if ($question['UsersAnswer']['answer_id'] == $question['Question']['Answer'][0]['id']) {
                    $correct++;
                }
            }
        }
        $result = round(($correct / $total_question) * 100, 1);
        return $result;
    }
    /**
     * @param null $test_id
     * @param string $test_type
     * @return array|null
     */
    private function get_all_flagged_questions($test_id = null, $test_type = ''){
        $this->loadModel('UsersAnswer');
        $query = [
            'recursive' => -1,
            'field' => ['UsersAnswer.is_flag','UsersAnswer.question_id'],
            'conditions' => [
                'UsersAnswer.user_id' => $this->get_user_id(),
                'UsersAnswer.test_id' => $test_id,
                'UsersAnswer.is_flag' => 1,
                'UsersAnswer.test_type' => $test_type,
            ],
        ];
        return $this->UsersAnswer->find('all',$query);
    }
    /**
     * @param null $test_id
     * @param bool $ques_text
     * @return array|null
     */
    private function _getQuestionByTest($test_id = null, $ques_text = false){
        if($ques_text) {
            $fields = ['TestsQuestion.question_id', 'Question.question'];
        } else {
            $fields = ['TestsQuestion.question_id'];
        }
        $query = [
            'conditions' => ['TestsQuestion.test_id' => $test_id, 'Test.status' => 1],
            'fields' => $fields,
        ];
        return $this->TestsQuestion->find('all', $query);
    }
    /**
     * @param array $questions_data_testwise
     * @return array
     */
    private function _create_jump_question_list($questions_data_testwise = []){
        $sl_no = 0;
        $questions_testwise = [];
        $questions_data_testwise = array_unique($questions_data_testwise);
        while($sl_no<count($questions_data_testwise)){
            #AuthComponent::_setTrace($questions_data_testwise);
            #$is_multiple = $this->_isMultipleQuestion(null, $questions_data_testwise[$sl_no]);
            $key = $questions_data_testwise[$sl_no];
            $result_sl = $sl_no + 1;
            $sl_no = $sl_no + 1;
            /* AuthComponent::_setTrace($key, false);
             AuthComponent::_setTrace($result_sl, false);*/
            $questions_testwise[$key] = $result_sl;
            //AuthComponent::_setTrace($questions_testwise, false);
        }
        //AuthComponent::_setTrace($questions_testwise);
        return $questions_testwise;
    }
    /**
     * @param null $test_id
     * @param int $limit
     * @param int $offset
     * @param null $total_question
     */
    public function ajax_held_exam($test_id = null, $limit = 1, $offset = 0, $total_question = null, $all_answered = false){
        $this->autoRender = false;
        $admin_id = AuthComponent::user('id')?AuthComponent::user('id'):1;
        if(!empty($admin_id)) {
            $user_id = $admin_id;
        } else {
            $user_id = $this->get_user_id();
        }
        $questions = $this->_getExamData($test_id, $limit, $offset, $user_id);
        $test_name = $questions['Test']['title'];
        $question_id = $questions['Question']['id'];
        $subject_id = $questions['Question']['subject_id'];
        /*AuthComponent::_setTrace($questions, false);
        AuthComponent::_setTrace($offset, false);*/
        $is_multiple = [];
        $find = 'first';

        $red_flag = '';
        $obj = new UsersAnswersController();
        /*End Initialization*/
        $is_exist = $this->_is_flag($test_id, $question_id, $user_id, 'mock_test');
        if(!empty($is_exist['UsersAnswer']['is_flag'])) {
            $red_flag = 'red-flag';
        }
        /*If already answered*/
        $answer_id = null;
        if(!empty($is_exist['UsersAnswer']['answer_id'])){
            $answer_id = $is_exist['UsersAnswer']['answer_id'];
        }
        /*Answer Submission and save into db*/
        if(!empty($this->request->data['answer'])){
            $answer_id_arr = explode('#', $this->request->data['answer']);
            if(count($answer_id_arr)>1) {
                $question_id_arr = explode('#',$this->request->data['current_question_id']);
                $ans_index_arr = explode('#',$this->request->data['ans_index']);
                $test_type = $this->request->data['test_type'];
                foreach($question_id_arr as $key => $current_question_id){
                    $answer_id = $answer_id_arr[$key];
                    $ans_index = $ans_index_arr[$key];
                    $obj->answer_submit($test_id, $current_question_id, $answer_id, $user_id, $ans_index, $test_type);
                }
            } else {
                $current_answer_id = $this->request->data['answer'];
                $current_question_id = $this->request->data['current_question_id'];
                $ans_index = $this->request->data['ans_index'];
                $test_type = $this->request->data['test_type'];
                $obj->answer_submit($test_id, $current_question_id, $current_answer_id, $user_id, $ans_index, $test_type);
            }
            #AuthComponent::_setTrace($questions);
        }
        /*When All question will be answered*//*When Exam will be finished*//*Result Calculation*/

        $my_answered_question = $obj -> no_of_answered_question($test_id, $user_id,'mock_test');
        $my_answered_question_count = count($my_answered_question);
        #AuthComponent::_setTrace($my_answered_question, false);
        if($my_answered_question_count>=$total_question || $all_answered) {
            $all_answered = true;
            $all_questions = $obj-> get_all_questions_after_exam($test_id, $user_id, 'mock_test');
            $result = $this->_calculateResult($all_questions, $total_question);
            $this->set(compact('all_questions','result'));
            $this->set(compact('find','all_answered','my_answered_question','total_question','offset','test_id','answer_id','my_answered_question_count'));
            $this->render('/Elements/result-page');
        }else{
            /*Handle flag in jump question list*/
            $submit_btn = count($my_answered_question) == $total_question-1 ? true : false;
            //$all_flagged_questions =  Hash::extract($this-> get_all_flagged_questions($test_id,'mock_test'), '{n}.UsersAnswer.question_id');
            //$questions_testwise = Hash::extract($this-> _getQuestionByTest($test_id), '{n}.TestsQuestion.question_id');
            $this->set(compact('questions','test_name','red_flag','all_flagged_questions','questions_testwise','submit_btn'));
            $this->set(compact('find', 'all_answered','my_answered_question','total_question','offset','test_id','answer_id','my_answered_question_count','answer_id_arr'));
            $this->render('/Elements/held_exam_content');
        }
    }
}
