<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                   
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!--<form role="form">-->
                   <?php echo $this->Form->create('Test'); ?>
						<fieldset>
							<legend><?php echo __('Admin Add Test'); ?></legend>
							<div class="form-group">

                                <?php  echo $this->Form->input('subject_id',array('class'=>'form-control','placeholder'=>'Fullname'));?>

                            </div>
							<div class="form-group">

                                <?php  echo $this->Form->input('group_id',array('class'=>'form-control','placeholder'=>'Fullname'));?>

                            </div>
							
							<div class="form-group">

                                <?php  echo $this->Form->input('title',array('class'=>'form-control','placeholder'=>'Fullname'));?>

                            </div>
							
							<div class="form-group">

                                <?php  echo $this->Form->input('instructions',array('class'=>'form-control','placeholder'=>'Fullname'));?>

                            </div>
							<div class="form-group">

                                <?php  echo $this->Form->input('test_time',array('class'=>'form-control','placeholder'=>'Fullname'));?>

                            </div>
							<div class="form-group">

                                <?php  echo $this->Form->input('status',array('class'=>'form-control','placeholder'=>'Fullname','color'=>'blue'));?>

                            </div>
							<div class="form-group">

                                <?php  echo $this->Form->input('start_date',array());?>

                            </div>
							
							<div class="form-group">

                                <?php  echo $this->Form->input('Question',array('class'=>'form-control','placeholder'=>'Fullname'));?>

                            </div>
							
						</fieldset>
					<?php echo $this->Form->end(__('Submit',array('class'=>'btn btn-primary'))); ?>
					
            </div>
			<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Tests'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Subject'), array('controller' => 'subjects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Results'), array('controller' => 'results', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Result'), array('controller' => 'results', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
