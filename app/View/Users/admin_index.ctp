<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Users</h3>
                    </div>
                    <!-- /.box-header -->



                    <!-- /.Search Box -->
                    <div class="box-body">
                        <div class="row">
                            <?php echo $this->Form->create('User', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label">Group</label>
                                    <div class="col-sm-10">
                                        <?php
                                        ?>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                            <!-- /.col -->
                          
                            </form>
                            <!-- /.col -->
                        </div>

                        <!-- /.Search Box -->






                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <th><?php echo $this->Paginator->sort('id'); ?></th>
								<th><?php echo $this->Paginator->sort('subject_id'); ?></th>
								<th><?php echo $this->Paginator->sort('group_id'); ?></th>
								<th><?php echo $this->Paginator->sort('title'); ?></th>
								<th><?php echo $this->Paginator->sort('instructions'); ?></th>
								<th><?php echo $this->Paginator->sort('test_time'); ?></th>
								<th><?php echo $this->Paginator->sort('status'); ?></th>
								<th><?php echo $this->Paginator->sort('start_date'); ?></th>
								<th><?php echo $this->Paginator->sort('created'); ?></th>
								<th><?php echo $this->Paginator->sort('modified'); ?></th>
								<th class="actions"><?php echo __('Actions'); ?></th>
                            </thead>
                            <tbody>
                            <?php foreach ($users as $user): ?>
							<tr>
								<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
								<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
								<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
								<td><?php echo h($user['User']['role']); ?>&nbsp;</td>
								<td><?php echo h($user['User']['password']); ?>&nbsp;</td>
								<td><?php echo h($user['User']['phone']); ?>&nbsp;</td>
								<td class="actions">
									<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id'])); ?>
									<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?>
									<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?>
								</td>
							</tr>
						<?php endforeach; ?>

                            </tbody>

                        </table>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>
                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                            echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                            echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                            ?>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>












