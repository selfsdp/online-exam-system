<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Results</h3>
                    </div>
                    <!-- /.box-header -->



                    <!-- /.Search Box -->
                    <div class="box-body">
                        <div class="row">
                            <?php echo $this->Form->create('User', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label">Group</label>
                                    <div class="col-sm-10">
                                        <?php
                                        ?>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                            <!-- /.col -->
                          
                            </form>
                            <!-- /.col -->
                        </div>

                        <!-- /.Search Box -->


                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            
                             <tr>
									<th><?php echo $this->Paginator->sort('id'); ?></th>
									<th><?php echo $this->Paginator->sort('title'); ?></th>
									<th><?php echo $this->Paginator->sort('status'); ?></th>
									<th><?php echo $this->Paginator->sort('created'); ?></th>
									<th><?php echo $this->Paginator->sort('modified'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
							</tr>
                            </thead>
                            <tbody>
                            		<?php foreach ($groups as $group): ?>
										<tr>
											<td><?php echo h($group['Group']['id']); ?>&nbsp;</td>
											<td><?php echo h($group['Group']['title']); ?>&nbsp;</td>
											<td><?php echo h($group['Group']['status']); ?>&nbsp;</td>
											<td><?php echo h($group['Group']['created']); ?>&nbsp;</td>
											<td><?php echo h($group['Group']['modified']); ?>&nbsp;</td>
											<td class="actions">
												<?php echo $this->Html->link(__('View'), array('action' => 'view', $group['Group']['id'])); ?>
												<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $group['Group']['id'])); ?>
												<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $group['Group']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $group['Group']['id']))); ?>
											</td>
										</tr>
									<?php endforeach; ?>

                            </tbody>

                        </table>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>
                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                            echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                            echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                            ?>
                        </ul>
                    </div>
					
					<div class="actions">
						<h3><?php echo __('Actions'); ?></h3>
						<ul>
							<li><?php echo $this->Html->link(__('New Group'), array('action' => 'add')); ?></li>
							<li><?php echo $this->Html->link(__('List Tests'), array('controller' => 'tests', 'action' => 'index')); ?> </li>
							<li><?php echo $this->Html->link(__('New Test'), array('controller' => 'tests', 'action' => 'add')); ?> </li>
						</ul>
					</div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

