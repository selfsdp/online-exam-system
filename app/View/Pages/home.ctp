<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="css/bodystyle.css" rel="stylesheet" />
<link href="css/default.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/fonts.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />

<
<style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 90%;
}
#register:hover{color:red;
				  text-decoration:underline;
				  cursor: pointer;}
#login:hover{color:red;
				  text-decoration:underline;
				  cursor: pointer;}

/* The Close Button */
.closereg,.closelog {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.closereg,.closelog:hover,
.closereg,.closelog:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

</style>

</head>
<body>
<div id="header-wrapper">
	<div id="header" class="">
		<div id="logo">
        	<span class="icon icon-cog"></span>

			<h1><a href="#">Online Exam System</a></h1>
			<h4><a href="#">Lict Project</a></h4>
		</div>
		
	</div>
</div>
<div class="wrapper1">
	<div id="banner" class="container"><img src="img/banner.jpg" width="1200" height="400" alt="" /></div>
	<div id="welcome" class="container">
    	
<div class="title">
	  <h2>Welcome to Online Exam System</h2>
		</div>
		<p>Here you can test your Skill .For pericipate you must <strong id="register">Register.
		</strong>If you already member of online Exam System. <strong id="login">Login</strong> and test your skill.</p>
	</div>
	
</div>

<!-- The Modal -->
<div id="loginform" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="closelog">&times;</span>
   
		  <section class="container">
				<div class="login">
				  <h1>Login to Web App</h1>
				  <form method="post" action="index.html">
					<p><input type="text" name="login" value="" placeholder="Username or Email"></p>
					<p><input type="password" name="password" value="" placeholder="Password"></p>
					<p class="remember_me">
					  <label>
						<input type="checkbox" name="remember_me" id="remember_me">
						Remember me on this computer
					  </label>
					</p>
					<p class="submit"><input type="submit" name="commit" value="Login"></p>
				  </form>
				</div>

				<div class="login-help">
				  <p>Forgot your password? <a href="#">Click here to reset it</a>.</p>
				</div>
		   </section>
	</div>
 
  

</div>
 
  

</div>

  <div id="registerform" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="closereg">&times;</span>
    <p> registration Some text in the Modal..</p>
  </div>
  

</div>

<script>
// Get the modal
var reginfo = document.getElementById('registerform');
var logininfo = document.getElementById('loginform');

// Get the button that opens the modal
var registerbtn = document.getElementById("register");
var loginbtn = document.getElementById("login");

// Get the <span> element that closes the modal
var regclose = document.getElementsByClassName("closereg")[0];
var logclose = document.getElementsByClassName("closelog")[0];

// When the user clicks the button, open the modal 
registerbtn.onclick = function() {
    reginfo.style.display = "block";
}

loginbtn.onclick = function() {
    logininfo.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
regclose.onclick = function() {
    reginfo.style.display = "none";
}
logclose.onclick = function() {
    logininfo.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == logininfo) {
        reginfo.style.display = "none";
    }
}
window.onclick = function(event) {
    if (event.target == reginfo) {
        logininfo.style.display = "none";
    }
}
</script>


<div id="copyright">
	<p>&copy; Untitled. All rights reserved. | Photos by Fotogrph | Design by.</p>
	<p><span class="icon icon-envelope"></span><span> info@onlineexam.com</span></p>
	<p><span class="icon icon-phone"></span><span> Telephone: +1 800 123 4657</span></p>
</div>
</body>
</html>
