<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 4/28/16
 * Time: 9:06 PM
 */
?>
<input type="hidden" id="action" value="<?php echo $this->params['action']?>">
<div class="cmsUsers col-md-12 col-sm-12 index">
    <div class="white">
        <h2></h2>

        <?php
        /**
         * Created by PhpStorm.
         * User: pc
         * Date: 5/2/16
         * Time: 5:28 PM
         */
        ?>
        <?php
        if (!$all_answered) {
            ?>
            <div class="col-md-12 exam-box">
                <div class="col-md-10 exam-box-child-1">
                    <div class="panel panel-primary js-exam-held">
                        <div class="panel-heading">
                            <div class="btn-group btn-block" role="group" aria-label="">
                                <button type="button" class="btn btn-default col-md-1" id="js-prev-btn"><span
                                        class="glyphicon glyphicon-arrow-left"></span></button>
                                <button type="button" class="btn btn-default col-md-3">
                                    <?php echo $offset < $total_question ? 'Ongoing ' . $test_name : 'Result' ?></button>
                                <button type="button" id="question-id-button" class="btn btn-default  col-md-3"
                                        value="<?php echo !empty($questions['Question']['id']) ? $questions['Question']['id'] : $questions[0]['Question']['id'] ?>">
                                        Question
                                        <span id="n_of_n">
                                        <?php
                                        $page_no = $offset;
                                        if(!empty($questions['Question']['id'])) {
                                            $current_page = $page_no + 1;
                                        } else{
                                            $current_page = ($page_no + 1) . ' - ' . ($page_no + 3);
                                        }
                                        echo $offset < $total_question ? $current_page : $total_question;
                                        ?>
                                        </span>
                                        of <?php
                                        echo $total_question;
                                        echo $offset < $total_question ? "" : " answered";
                                        ?>
                                    <span class="glyphicon glyphicon-flag js-flag <?php echo $red_flag ?>"
                                          style="margin-left: 30px"></span>
                                </button>
                                <!-- Single button -->
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        <span class="js-question-no">Jump Question</span> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu common-jump-question-list-height" id="jump-question-list">
                                        <?php
                                        foreach ($questions_testwise as $real_q_id => $ques_id) { ###### $ques_id sl no here
                                            //$ques_id = $questions_testwise[$ques - 1];
                                            $real_q_id = array_shift(array_filter(explode('#',$real_q_id)));
                                            if (in_array($real_q_id, $all_flagged_questions)) {
                                                echo '<li id="' . $ques_id . '" class="js-question-li" rel="' . $real_q_id . '"><a href="javascript:void(0)">' . $ques_id . '<span class="glyphicon glyphicon-flag js-flag red-flag" style="margin-left: 40%"></span></a></li>';
                                            } else {
                                                echo '<li id="' . $ques_id . '" class="js-question-li" rel="' . $real_q_id . '"><a href="javascript:void(0)">' . $ques_id . '</a></li>';
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <button type="button" class="btn btn-default  col-md-2" id="timer"> 00:00 </button>
                                <button type="button" class="btn btn-default  col-md-1" id="js-next-btn"><span
                                        class="glyphicon glyphicon-arrow-right"></span></button>
                            </div>
                        </div>
                        <div class="panel-body js-question-box">
                            <?php
                            echo $this->element('held_exam_content');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="panel panel-info exam-box-child-2">
                        <div class="panel-heading text-center">Dashboard</div>
                        <div id="sidebar" class="panel-body yellow-panel">
                            <table class="table table-bordered table-striped js-dashboard-table">
                                <tbody>
                                <?php
                                #AuthComponent::_setTrace($questions_testwise);
                                foreach ($questions_testwise as $qu_no => $item) { #item => sl no here
                                    $question_ids_dashboard = array_filter(explode('#',$qu_no));
                                    $ques_id = array_shift(array_filter(explode('#',@$qu_no)));
                                    $flag = in_array($ques_id, $all_flagged_questions) ? true : false;
                                    #AuthComponent::_setTrace($question_ids_dashboard, false);
                                    if(count($question_ids_dashboard) > 1) {
                                        $mul_ans_data = [];
                                        foreach ($question_ids_dashboard as $q_id) {
                                            $data = AppHelper::search($my_answered_question, 'question_id', $q_id);
                                            if(!empty($data)) {
                                                $mul_ans_data[] = $data[0]['ans_index'];
                                            }
                                        }
                                        if(!empty($mul_ans_data)) {
                                            $ans_index = implode(',', $mul_ans_data);
                                        } else {
                                            $ans_index = '-';
                                        }
                                        #AuthComponent::_setTrace($ans_index);
                                    } else {
                                        $ans_data = AppHelper::search($my_answered_question, 'question_id', $ques_id);
                                        $ans_index = !empty($ans_data) ? $ans_data[0]['ans_index'] : '-';
                                    }
                                    ?>
                                    <tr>
                                        <td align="center" width="65"><a href="javascript:void(0)" class="intlnk"
                                                                         value="0"><?php echo $item ?></a></td>
                                        <td align="center" width="60" id="js-ans-index-<?php echo $ques_id?>"><?php echo $ans_index?></td>
                                        <td align="center" width="40" id="js-flag-<?php echo $ques_id?>"><?php echo $flag==true ? '<span class="glyphicon glyphicon-flag red-flag"></span>' : '-' ?></td>
                                    </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        } else { #When Exam will finish and need to show result
            ?>
            <script type="text/javascript">
                $('#exam-menu').hide();
                $('#result-menu').show();
            </script>
            <div class="panel panel-info" id="js-display-result-box">
                <div class="panel-heading">
                    <h1 class="panel-title">
                        <span class="glyphicon glyphicon-stats"></span>
                        <?php
                        if($my_answered_question_count == $total_question) {
                            ?>
                            You attempted all <?php echo $total_question ?> questions, scoring <strong><?php echo $result; ?>%</strong>
                        <?php
                        } else{
                            ?>
                            You attempted <?php echo $my_answered_question_count?> of all <?php echo $total_question ?> questions, scoring <strong><?php echo $result; ?>%</strong>
                        <?php
                        }
                        ?>
                    </h1>
                </div>
                <div class="panel-body">
                    <div class="panel panel-default">
                        <div class="panel-heading result-page">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-justified" role="tablist">
                                <li role="presentation" class="active col-md-3 bg-primary"><a href="#home" aria-controls="home"
                                                                                              role="tab"
                                                                                              data-toggle="tab"><span
                                            class="glyphicon glyphicon-th-list"></span> All Questions</a></li>
                                <li role="presentation" class="col-md-3 bg-primary"><a href="#profile" aria-controls="profile"
                                                                                       role="tab"
                                                                                       data-toggle="tab"><span
                                            class="glyphicon glyphicon-ok"></span> Correct Questions</a></li>
                                <li role="presentation" class="col-md-3 bg-primary"><a href="#messages" aria-controls="messages"
                                                                                       role="tab"
                                                                                       data-toggle="tab">
                                        <span class="glyphicon glyphicon-remove"></span> Incorrect Questions
                                    </a></li>
                                <li role="presentation" class="col-md-3 bg-primary"><a href="#settings" aria-controls="settings"
                                                                                       role="tab"
                                                                                       data-toggle="tab"><span
                                            class="glyphicon glyphicon-flag js-flag"></span> Flag Questions</a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <table class="table table-bordered table-striped answered-question-table">
                                        <tbody>
                                        <?php
                                        foreach ($all_questions as $key => $question) {
                                            $sl_no = $key + 1;
                                            if ($question['UsersAnswer']['answer_id'] == $question['Question']['Answer'][0]['id']) {
                                                $res_class = "success";
                                                $img_src = $this->Html->url('/img/small_tick.gif');
                                            } else {
                                                $res_class = "danger";
                                                $img_src = $this->Html->url('/img/small_cross.gif');
                                            }
                                            ?>
                                            <tr class="<?php echo $res_class ?>">
                                                <td align="center"><?php echo $sl_no; ?></td>
                                                <td><b>Malignant otitis
                                                        externa</b><br><?php echo $question['Question']['question'] ?>
                                                </td>
                                                <td width="30" align="center"><img
                                                        src="<?php echo $img_src ?>"></td>
                                                <td align="center"><a href="<?php echo $this->Html->url('/tests_questions/q_bank_practice/' . $test_id . '/1/' . $key . '/1/mock_test', true) ?>"
                                                                      class="btn btn-primary">Review</a>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">
                                    <table class="table table-bordered table-striped answered-question-table">
                                        <tbody>
                                        <?php
                                        foreach ($all_questions as $key => $question) {
                                            $sl_no = $key + 1;
                                            if ($question['UsersAnswer']['answer_id'] == $question['Question']['Answer'][0]['id']) {
                                                $res_class = "success";
                                                $img_src = 'http://d2zgo9qer4wjf4.cloudfront.net/css/small_tick.gif';
                                                ?>
                                                <tr class="<?php echo $res_class ?>">
                                                    <td align="center"><?php echo $sl_no; ?></td>
                                                    <td><b>Malignant otitis
                                                            externa</b><br><?php echo $question['Question']['question'] ?>
                                                    </td>
                                                    <td width="30" align="center"><img
                                                            src="<?php echo $img_src ?>"></td>
                                                    <td align="center"><a href="<?php echo $this->Html->url('/tests_questions/q_bank_practice/' . $test_id . '/1/' . $key . '/1/mock_test', true) ?>"
                                                                          class="btn btn-primary">Review</a>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="messages">
                                    <table class="table table-bordered table-striped answered-question-table">
                                        <tbody>
                                        <?php
                                        foreach ($all_questions as $key => $question) {
                                            $sl_no = $key + 1;
                                            if ($question['UsersAnswer']['answer_id'] != $question['Question']['Answer'][0]['id']) {
                                                $res_class = "danger";
                                                $img_src = 'http://d2zgo9qer4wjf4.cloudfront.net/css/small_cross.gif';
                                                ?>
                                                <tr class="<?php echo $res_class ?>">
                                                    <td align="center"><?php echo $sl_no; ?></td>
                                                    <td><b>Malignant otitis
                                                            externa</b><br><?php echo $question['Question']['question'] ?>
                                                    </td>
                                                    <td width="30" align="center"><img
                                                            src="<?php echo $img_src ?>"></td>
                                                    <td align="center"><a href="<?php echo $this->Html->url('/tests_questions/q_bank_practice/' . $test_id . '/1/' . $key . '/1/mock_test', true) ?>"
                                                                          class="btn btn-primary">Review</a>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="settings">
                                    <table class="table table-bordered table-striped answered-question-table">
                                        <tbody>
                                        <?php
                                        foreach ($all_questions as $key => $question) {
                                            $sl_no = $key + 1;
                                            if (!empty($question['UsersAnswer']['is_flag'])) {
                                                if ($question['UsersAnswer']['answer_id'] == $question['Question']['Answer'][0]['id']) {
                                                    $res_class = "success";
                                                    $img_src = 'http://d2zgo9qer4wjf4.cloudfront.net/css/small_tick.gif';
                                                    ?>
                                                <?php
                                                } else {
                                                    $res_class = "danger";
                                                    $img_src = 'http://d2zgo9qer4wjf4.cloudfront.net/css/small_cross.gif';
                                                }
                                                ?>
                                                <tr class="<?php echo $res_class ?>">
                                                    <td align="center"><?php echo $sl_no; ?></td>
                                                    <td><b>Malignant otitis
                                                            externa</b><br><?php echo $question['Question']['question'] ?>
                                                    </td>
                                                    <td width="30" align="center"><img
                                                            src="<?php echo $img_src ?>"></td>
                                                    <td align="center"><a href="<?php echo $this->Html->url('/tests_questions/q_bank_practice/' . $test_id . '/1/' . $key . '/1/mock_test', true) ?>"
                                                                          class="btn btn-primary">Review</a>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                        }

                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        if(isset($remaining_test_time) && $remaining_test_time !=null) {
            $test_time = $remaining_test_time;
        } else {
            $test_time = isset($questions['Test']['test_time']) ? $questions['Test']['test_time'] : 0;
        }
        ?>
        <script type="text/javascript">
            var test_time = "<?php echo $test_time?>";
        </script>
        <style type="text/css">
            #timer {
                /*color: fuchsia;*/
                font-weight: 600;
            }
        </style>
        <script type="text/javascript">
            $(function () {
                //Dashboard scroll
                $('.exam-box-child-2').slimScroll({
                    height: $('.exam-box-child-1').height() - 17 + 'px'
                });

                /*Timer CountDown*/
                if(test_time>0){            // this check needed
                    var countdown = parseInt(test_time) * 1000;
                    var timerId = setInterval(function () {
                        countdown -= 1000;
                        var hour = Math.floor((countdown / (1000 * 60 * 60)) % 24);
                        var min = Math.floor((countdown / 1000 / 60) % 60);
                        var sec = Math.floor((countdown / 1000) % 60);  //correct
                        if (countdown <= 0) {
                            clearInterval(timerId);
                            swal({
                                title: "Mock Test Info",
                                text: "Your allocated time has expired. Please Click Ok to proceed the result page.",
                                type: "error",
                                showCancelButton: false, closeOnConfirm: true, showLoaderOnConfirm: true
                            }, function () {
                                $('#exam-menu').hide();
                                $('#result-menu').show();

                                var test_id = $('#test_id').val();
                                var total_question = $('#total_question').val();
                                var offset = $('#offset').val();

                                var  user_id = $('#user_id').val();
                                var exit_time = $('#timer').text();
                                var data = {
                                    'data[Saveandexit][user_id]': user_id,
                                    'data[Saveandexit][test_id]': test_id,
                                    'data[Saveandexit][exit_time]': 1
                                };

                                $.post(ROOT + 'saveandexits/save_and_exit',data, function(response){
                                    //console.log(response)
                                    if(response.success) {
                                        $.get(ROOT + 'tests_questions/ajax_held_exam/'+test_id+'/1/'+offset+'/'+total_question+'/'+true, function(response){
                                            $('.exam-box').html(response);
                                        });
                                    }  else{
                                        sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
                                    }
                                },'json');
                            });
                        } else {
                            if (hour > 0) {
                                $("#timer").html(hour + ':' + min + ":" + sec);
                            } else {
                                $("#timer").html(min + ":" + sec);
                            }
                        }
                    }, 1000); //1000ms. = 1sec.
                }
            });
        </script>

        <!--<div class="js-exam-held">
            <?php
/*                echo $this->element('held_exam_content');
            */?>
        </div>-->
    </div>
</div>