<div class="usersAnswers view">
<h2><?php echo __('Users Answer'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($usersAnswer['UsersAnswer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersAnswer['User']['id'], array('controller' => 'users', 'action' => 'view', $usersAnswer['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Test'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersAnswer['Test']['title'], array('controller' => 'tests', 'action' => 'view', $usersAnswer['Test']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersAnswer['Question']['id'], array('controller' => 'questions', 'action' => 'view', $usersAnswer['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersAnswer['Answer']['id'], array('controller' => 'answers', 'action' => 'view', $usersAnswer['Answer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Test Type'); ?></dt>
		<dd>
			<?php echo h($usersAnswer['UsersAnswer']['test_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ans Index'); ?></dt>
		<dd>
			<?php echo h($usersAnswer['UsersAnswer']['ans_index']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Flag'); ?></dt>
		<dd>
			<?php echo h($usersAnswer['UsersAnswer']['is_flag']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ques Offset'); ?></dt>
		<dd>
			<?php echo h($usersAnswer['UsersAnswer']['ques_offset']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($usersAnswer['UsersAnswer']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($usersAnswer['UsersAnswer']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Users Answer'), array('action' => 'edit', $usersAnswer['UsersAnswer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Users Answer'), array('action' => 'delete', $usersAnswer['UsersAnswer']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $usersAnswer['UsersAnswer']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Users Answers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users Answer'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tests'), array('controller' => 'tests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Test'), array('controller' => 'tests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); ?> </li>
	</ul>
</div>
